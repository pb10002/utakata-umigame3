var express = require('express');
var socket = require('socket.io');
var db = require('./redis_client');
var app = express();
var messages = [];
var id = 0;
server = app.listen(8080, function(){
  console.log('server is running on port 8080')
});
io = socket(server);

io.on('connection', (socket) => {
  socket.on('fetchLobby', function(){
    messages = [];
    db.getAll((msgs)=>{
      for(var key in msgs){
        messages.push(JSON.parse(msgs[key]));
      }
      io.emit('lobbyChat', messages);
    });
  });
  socket.on('removeAll', function(){
    db.removeAll();
    messages = [];
    io.emit('lobbyChat', []);
  });
  socket.on('message', function(data){
    messages = [];
    db.getAll((msgs)=>{
      for(var key in msgs){
        messages.push(JSON.parse(msgs[key]));
      }
      data.id = id++;
      console.log(data);
      messages.push(data);
      db.addData(data);
      io.emit('lobbyChat', messages);
    });
  })
});
